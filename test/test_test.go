// Copyright 2021 Emiliano Vaz Fraga

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 	http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package test_test

import (
	"fmt"
	"testing"

	"gitlab.com/EmilianoFraga/munt/test"
)

func TestIsTrue(t *testing.T) {
	actual := true

	if ok, msg := test.IsTrue(actual, "actual"); expectingNoError(ok, msg) {
		t.Fail()
	}

	actual = false
	if ok, msg := test.IsTrue(actual, "actual"); expectingError(ok, msg) {
		t.Fail()
	}
}

func TestIsFalse(t *testing.T) {
	actual := false

	if ok, msg := test.IsFalse(actual, "actual"); expectingNoError(ok, msg) {
		t.Fail()
	}

	actual = true
	if ok, msg := test.IsFalse(actual, "actual"); expectingError(ok, msg) {
		t.Fail()
	}
}

func TestIntEquals(t *testing.T) {
	var actual int = 1
	var expected int = 1

	if ok, msg := test.IntEquals(actual, expected, "actual"); expectingNoError(ok, msg) {
		t.Fail()
	}

	actual = expected + 1
	if ok, msg := test.IntEquals(actual, expected, "actual"); expectingError(ok, msg) {
		t.Fail()
	}
}

func TestStringEquals(t *testing.T) {
	var actual string = "abc"
	var expected string = "abc"

	if ok, msg := test.StringEquals(actual, expected, "actual"); expectingNoError(ok, msg) {
		t.Fail()
	}

	actual = expected + "d"
	if ok, msg := test.StringEquals(actual, expected, "actual"); expectingError(ok, msg) {
		t.Fail()
	}
}

func TestByteSliceEquals(t *testing.T) {
	var actual []byte = []byte{1, 2, 3}
	var expected []byte = []byte{1, 2, 3}

	if ok, msg := test.ByteSliceEquals(actual, expected, "actual"); expectingNoError(ok, msg) {
		t.Fail()
	}

	actual = []byte{5, 2, 3}
	if ok, msg := test.ByteSliceEquals(actual, expected, "actual"); expectingError(ok, msg) {
		t.Fail()
	}
}

func TestIsNil(t *testing.T) {
	if ok, msg := test.IsNil(nil, "actual"); expectingNoError(ok, msg) {
		t.Fail()
	}

	var actual *byte = nil

	if ok, msg := test.IsNil(actual, "actual"); expectingNoError(ok, msg) {
		t.Fail()
	}

	var x byte = 1
	actual = &x
	if ok, msg := test.IsNil(actual, "actual"); expectingError(ok, msg) {
		t.Fail()
	}
}

func TestIsNotNil(t *testing.T) {
	if ok, msg := test.IsNotNil(nil, "actual"); expectingError(ok, msg) {
		t.Fail()
	}

	var actual *byte = nil

	if ok, msg := test.IsNotNil(actual, "actual"); expectingError(ok, msg) {
		t.Fail()
	}

	var x byte = 1
	actual = &x
	if ok, msg := test.IsNotNil(actual, "actual"); expectingNoError(ok, msg) {
		t.Fail()
	}
}

func TestNoError(t *testing.T) {
	if ok, msg := test.NoError(nil); expectingNoError(ok, msg) {
		t.Fail()
	}

	var actual error = nil

	if ok, msg := test.NoError(actual); expectingNoError(ok, msg) {
		t.Fail()
	}

	actual = fmt.Errorf("this is an error")
	if ok, msg := test.NoError(actual); expectingError(ok, msg) {
		t.Fail()
	}
}

func TestErrorMessageContains(t *testing.T) {
	if ok, msg := test.ErrorMessageContains(nil, ""); expectingError(ok, msg) {
		t.Fail()
	}

	var actual error = nil

	if ok, msg := test.ErrorMessageContains(actual, ""); expectingError(ok, msg) {
		t.Fail()
	}

	expectedMsg := "this is an error"
	actual = fmt.Errorf(expectedMsg)
	if ok, msg := test.ErrorMessageContains(actual, expectedMsg); expectingNoError(ok, msg) {
		t.Fail()
	}

	expectedMsg = "different message"
	if ok, msg := test.ErrorMessageContains(actual, expectedMsg); expectingError(ok, msg) {
		t.Fail()
	}
}

func TestErrorMessageDoesNotContain(t *testing.T) {
	if ok, msg := test.ErrorMessageDoesNotContain(nil, ""); expectingError(ok, msg) {
		t.Fail()
	}

	var actual error = nil

	if ok, msg := test.ErrorMessageDoesNotContain(actual, ""); expectingError(ok, msg) {
		t.Fail()
	}

	expectedMsg := "this is an error"
	actual = fmt.Errorf(expectedMsg)
	if ok, msg := test.ErrorMessageDoesNotContain(actual, expectedMsg); expectingError(ok, msg) {
		t.Fail()
	}

	expectedMsg = "different message"
	if ok, msg := test.ErrorMessageDoesNotContain(actual, expectedMsg); expectingNoError(ok, msg) {
		t.Fail()
	}
}

func expectingNoError(ok bool, msg string) bool {
	return !ok || len(msg) > 0
}

func expectingError(ok bool, msg string) bool {
	return ok || len(msg) < 1
}
