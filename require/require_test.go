// Copyright 2021 Emiliano Vaz Fraga

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 	http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package require_test

import (
	"fmt"
	"testing"

	"gitlab.com/EmilianoFraga/munt/require"
)

func TestIsTrue(t *testing.T) {
	actual := true

	require.IsTrue(t, actual, "actual")
}

func TestIsFalse(t *testing.T) {
	actual := false

	require.IsFalse(t, actual, "actual")
}

func TestIntEquals(t *testing.T) {
	var actual int = 1
	var expected int = 1

	require.IntEquals(t, actual, expected, "actual")
}

func TestStringEquals(t *testing.T) {
	var actual string = "abc"
	var expected string = "abc"

	require.StringEquals(t, actual, expected, "actual")
}

func TestByteSliceEquals(t *testing.T) {
	var actual []byte = []byte{1, 2, 3}
	var expected []byte = []byte{1, 2, 3}

	require.ByteSliceEquals(t, actual, expected, "actual")
}

func TestIsNil(t *testing.T) {
	require.IsNil(t, nil, "actual")

	var actual *byte = nil

	require.IsNil(t, actual, "actual")
}

func TestIsNotNil(t *testing.T) {
	var x byte = 1
	var actual = &x
	require.IsNotNil(t, actual, "actual")
}

func TestNoError(t *testing.T) {
	require.NoError(t, nil)

	var actual error = nil

	require.NoError(t, actual)
}

func TestErrorMessageContains(t *testing.T) {
	expectedMsg := "this is an error"
	actual := fmt.Errorf(expectedMsg)
	require.ErrorMessageContains(t, actual, expectedMsg)
}

func TestErrorMessageDoesNotContain(t *testing.T) {
	actual := fmt.Errorf("this is an error")
	expectedMsg := "different message"
	require.ErrorMessageDoesNotContain(t, actual, expectedMsg)
}
