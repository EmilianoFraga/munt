// Copyright 2021 Emiliano Vaz Fraga

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 	http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package require

import (
	"testing"

	"gitlab.com/EmilianoFraga/munt/test"
)

func IsTrue(t *testing.T, actual bool, variableName string) {
	if ok, msg := test.IsTrue(actual, variableName); !ok {
		t.Fatalf(msg)
	}
}

func IsFalse(t *testing.T, actual bool, variableName string) {
	if ok, msg := test.IsFalse(actual, variableName); !ok {
		t.Fatalf(msg)
	}
}

func IntEquals(t *testing.T, actual int, expected int, variableName string) {
	if ok, msg := test.IntEquals(actual, expected, variableName); !ok {
		t.Fatalf(msg)
	}
}

func StringEquals(t *testing.T, actual string, expected string, variableName string) {
	if ok, msg := test.StringEquals(actual, expected, variableName); !ok {
		t.Fatalf(msg)
	}
}

func ByteSliceEquals(t *testing.T, actual []byte, expected []byte, variableName string) {
	if ok, msg := test.ByteSliceEquals(actual, expected, variableName); !ok {
		t.Fatalf(msg)
	}
}

func IsNil(t *testing.T, something interface{}, variableName string) {
	if ok, msg := test.IsNil(something, variableName); !ok {
		t.Fatalf(msg)
	}
}

func IsNotNil(t *testing.T, something interface{}, variableName string) {
	if ok, msg := test.IsNotNil(something, variableName); !ok {
		t.Fatalf(msg)
	}
}

func NoError(t *testing.T, err error) {
	if ok, msg := test.NoError(err); !ok {
		t.Fatalf(msg)
	}
}

func ErrorMessageContains(t *testing.T, err error, substring string) {
	if ok, msg := test.ErrorMessageContains(err, substring); !ok {
		t.Fatalf(msg)
	}
}

func ErrorMessageDoesNotContain(t *testing.T, err error, substring string) {
	if ok, msg := test.ErrorMessageDoesNotContain(err, substring); !ok {
		t.Fatalf(msg)
	}
}
